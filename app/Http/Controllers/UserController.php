<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\TaskController;
use Illuminate\Pagination\Paginator;

class UserController extends Controller
{

    public function reg()
    {
        if(session('uname')){
            echo "Zaten Giriş Yapmışsınız.";
            die();
        }
        return view('users.register');
    }

    public function register()
    {
   request()->validate([
            'uname' => 'required',
            'pwd' => 'required',
            'pwdconfirm' => 'required'
        ]);
        if(request('pwd') == request('pwdconfirm')){
            if(User::where('uname', '=', request('uname'))->exists())
            {
                echo "Kullanıcı adı sistemde zaten var.";
                die();
            }
        User::create([
            'uname' => request('uname'),
            'pwd' => request('pwd')
        ]);
        return redirect('/tasks');
        }else{
            echo "Şifreler aynı değil...";
        }
    }

    public function log(){
        if(session('uname'))
        {
        echo "Zaten giriş yapılmış";
        die();
        }
        return view('users.login');
    }

    public function login(Request $req)
    {        
        $data = $req->input();
        if (User::where('uname', '=', $data['uname'])->exists())
        {
            $user = User::where('uname', '=', $data['uname'])->first();
            $tip = $user->tip;
            $pass = $user->pwd;
            if($data['pwd'] == $pass){
                $req->session()->put('uname', [$data['uname'], $tip]);
                return redirect('/');
            }else
            {
                echo 'şifre hatalı!';
                echo session('uname');
            }
        }else
            {
            echo "Kullanıcı adı veya şifre hatalı!";
        }
    }


    public function admin()
    {
        return view('users.admin');
    }

    public function kullanicilar()
    {
        $sayfa = request()->sayfa;
        $fetch = User::all();

        $lenus = count($fetch);
        $sayfasayus = ceil($lenus/5);
        //$array = [$sayfa, $fetch, $lenus,$sayfasayus, $sayfaus]; 
        return view('users.kullanicilar')->with('data', [['sayfa' => "$sayfa"], ['sayfasayus' => "$sayfasayus"], ['fetch' => "$fetch"], ['lenus' => "$lenus"]]);
    }

    public function kullanicilarpag()
    {
        $session = session('uname');
        if($session[1] !== 'admin'){
          die('admin deyilsin');
        }
        $fetch = User::paginate(3);
        return view('users.kulpag')->with(['fetch'=>$fetch]);
    }
}
