<?php

namespace App\Http\Controllers;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        return view('tasks.index', ['tasks'=>$tasks]);
    }

    public function show(Task $task){
        return view('tasks.show', ['task'=>$task]);
    }

    public function create()
    {
       
        if(!(session('uname')[1]=='admin')){
            echo "Bu sayfa adminler içindir";
            die();
        }
        return view('tasks.create');
    }

    public function edit(Task $task)
    {
        if(!(session('uname')[1]=='admin')){
            echo "Bu sayfa adminler içindir";
            die();
        }
        return view('tasks.edit', ['task' => $task]);
    }

    public function update(Task $task)
    {
        
        request()->validate([
            'task' => 'required',
            'durum' => 'required',
            'onem' => 'nullable'
        ]);

        $task->update([
            'task' => request('task'),
            'durum' => request('durum'),
            'onem'  => request('onem')
        ]);
        return redirect('/tasks');
    }

    public function store()
    {
        request()->validate([
            'task' => 'required',
            'durum' => 'required',
            'onem' => 'required'
        ]);

        Task::create([
            'task' => request('task'),
            'durum' => request('durum'),
            'onem'  => request('onem')
        ]);
        return redirect('/tasks');
    }

    public function delete(Task $task)
    {
        $task->delete();
        return redirect('/tasks');
    }

    public function tasklar()
    {
        $sayfa = request()->sayfa;
        $fetch = Task::all();
        $lenus = count($fetch);
        $sayfasayus = ceil($lenus/5); 
        return view('users.tasklar')->with('data', [['sayfa' => "$sayfa"], ['sayfasayus' => "$sayfasayus"], ['fetch' => "$fetch"], ['lenus' => "$lenus"]]);
    }

    public function tasklarpag()
    {
        $session = session('uname');
if($session[1] !== 'admin'){
  die('admin deyilsin');
}
        $fetch = Task::paginate(3);
        return view('users.taskpag')->with(['fetch'=>$fetch]);
    }
}