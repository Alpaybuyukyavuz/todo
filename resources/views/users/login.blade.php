<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
</head>
<body>
    <h2>Giriş Yapınız</h2>
    <form action="/login" method="POST">
        @csrf
        <input type="text" name="uname" placeholder="Kullanıcı Adınız">
        <input type="password" name="pwd" placeholder="Şifreniz">
        <input type="submit" name="giris" value="Giriş">
    </form>
</body>
</html>