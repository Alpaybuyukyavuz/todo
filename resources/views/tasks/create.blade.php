<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <div style="width: 900px;" class="container max-w-full mx-auto pt-4">
        <form action="/tasks/create" method="POST">
            @csrf
            <input type="text" name="task" placeholder="Ne yapılacak?">
            <input type="text" name="durum" placeholder="0-1?"><br><br><br><br>
            <input type="number" name="onem" min="1" max="3" placeholder="Önem seviyesi:[1-3]" style="width: 200px;">
            <input type="submit" name="submit" value="Ekle">
        </form>
    </div>
</body>
</html>