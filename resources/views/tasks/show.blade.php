<!DOCTYPE html>
<html lang="en">
<head>

    <title>{{ $task->id }} ID'li Task</title>
</head>
<body>
    <h2>Task ID'si: {{ $task->id }}</h2>
    <table>
        <tr>
            <th>Task ID</th>
            <th>Task</th>
            <th>Task Durumu</th>
            <th>Task Önemi</th>
          </tr>
            <tr>
              <td>|{{ $task->id }}|</td>
              <td>|{{ $task->task }}|</td>
              <td>|{{ $task->durum }}|</td>
              <td>|{{ $task->onem }}|</td>
            </tr>
    </table>
    <br>
    <br>
    <br>
    <br>
    <?php if(session('uname')){
      $session = session('uname');
      if($session['1'] == "admin") {?>
      <a href="/tasks/{{ $task->id }}/edit"> Taskı Düzenlemek İçin Tıkla</td></a>
      <?php }}?>
</body>
</html>