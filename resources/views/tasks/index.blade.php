<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tasklar</title>
</head>
<body>
    <?php if(session('uname')){
    $session = session('uname');
    if($session['1'] == "admin") {?>
    <a href="/admin">Admin Paneli</a>
    <h2><a href="/tasks/create"> Task Eklemek İçin Tıkla</td></a></h2>
    <?php }}?>
<?php if(!session('uname'))
{?>
    <a href="/login">Giriş Yap</a>
    <a href="/register">Kaydol</a>
<?php }elseif(session('uname')){?>
     <a href="/logout">Çıkış Yap</a><?php } echo session('tip');?>
    
    <br>
    <br>
    <table>
        <tr>
            <th>Task ID</th>
            <th>Task</th>
            <th>Task Durumu</th>
            <th>Task Önemi</th>
          </tr>
    <?php foreach ($tasks as $l) {?>
        <?php if($l->durum == 0){?>
            <tr>
              <td>|<a href="/tasks/{{ $l->id }}"> {{ $l->id }}|</td></a>
              <td>|{{ $l->task }}|</td>
              <td>|{{ $l->durum }}|</td>
              <td>|{{ $l->onem }}|</td>
            </tr>
            <?php }elseif($l->durum == 1){?>
                <tr>
                    <td>|<del><a href="/tasks/{{ $l->id }}"> {{ $l->id }}|</del></td></a>
                    <td>|<del>{{ $l->task }}</del>|</td>
                    <td>|<del>{{ $l->durum }}</del>|</td>
                    <td>|<del>{{ $l->onem }}</del>|</td>
                  </tr>
                <?php }?>
    <?php }?>
    </table>
</body>
</html>