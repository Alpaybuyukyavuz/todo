<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use App\Models\Task;
use Illuminate\Database\Connectors\PostgresConnector;
use phpDocumentor\Reflection\DocBlock\Tag;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TaskController::class, 'index']);
Route::get('/register', [UserController::class, 'reg']);
Route::post('/register', [UserController::class, 'register']);
Route::get('/tasks', [TaskController::class, 'index']);
Route::get('/tasks/create', [TaskController::class, 'create']);
Route::post('/tasks/create', [TaskController::class, 'store']);
Route::get('/tasks/{task}', [TaskController::class, 'show']);
Route::get('/tasks/{task}/edit', [TaskController::class, 'edit']);
Route::post('/tasks/{task}/edit', [TaskController::class, 'update']);
Route::delete('/tasks/{task}/edit', [TaskController::class, 'delete']);
Route::get('/login', [UserController::class, 'log']);
Route::post('/login', [UserController::class, 'login']);
Route::get('/admin', [UserController::class, 'admin']);
//Route::get('/kullanicilar/{sayfa}', [UserController::class, 'kullanicilar']);
//Route::get('/tasklar/{sayfa}', [TaskController::class, 'tasklar']);
Route::get('kulpag', [UserController::class, 'kullanicilarpag']);
Route::get('taskpag', [TaskController::class, 'tasklarpag']);



Route::get('/logout', function(){
    if(session()->pull('uname'))
    {
        session()->pull('uname');
    }else
    {
        echo "Session yok";
    }
    return redirect('login');
});